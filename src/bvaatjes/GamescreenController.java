package bvaatjes;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class GamescreenController {
    @FXML
    private Text title;
    @FXML
    private Text description;
    @FXML
    private Button homeButton;
    @FXML
    private Button rankingButton;

    @FXML
    protected String homeButtonAction() {
        return changeScene("homescreen");
    }

    @FXML
    private String changeScene(String scene) {
        try {
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) title.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length() - 6) + " screen");
            return "Switched to " + scene;
        } catch (IOException IOE) {
            return "Whoops, couldn't load the new scene...";
        }
    }
}

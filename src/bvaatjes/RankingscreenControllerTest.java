package bvaatjes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RankingscreenControllerTest {

    @Test
    void homeButtonAction() {
        assertEquals("Switched to homescreen", "Switched to homescreen");
    }

    @Test
    void rankingButtonAction() {
        assertEquals("Switched to rankingscreen", "Switched to rankingscreen");
    }
}
package bvaatjes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GamescreenControllerTest {

    @Test
    void homeButtonAction() {
        assertEquals("Switched to homescreen", "Switched to homescreen");
    }
}
package bvaatjes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class OptionscreenController {
    @FXML
    private Text title;
    @FXML
    private ComboBox<String> sizeChosen;
    @FXML
    private ComboBox<String> timeChosen;
    @FXML
    private ComboBox<String> photoChosen;
    @FXML
    private ToggleButton timeTrail;
    @FXML
    private ToggleButton defaultPhoto;
    @FXML
    private ToggleButton loadPhoto;
    @FXML
    private ToggleButton firstPlayer;
    @FXML
    private ToggleButton firstWASD;
    @FXML
    private ToggleButton firstKeys;
    @FXML
    private ToggleButton firstMouse;
    @FXML
    private ToggleButton secondPlayer;
    @FXML
    private ToggleButton secondWASD;
    @FXML
    private ToggleButton secondKeys;
    @FXML
    private ToggleButton secondMouse;
    @FXML
    private Button browsePhoto;
    @FXML
    private TextField firstPlayerName;
    @FXML
    private TextField secondPlayerName;
    @FXML
    private Button homeButton;
    @FXML
    private Button playButton;
    @FXML
    private HBox timeTrailBox;
    private ComboBox<String> times;

    @FXML
    protected void initialize() {
        ObservableList<String> options =
                FXCollections.observableArrayList("00:01:00", "00:03:00", "00:09:00");
        times = new ComboBox<>(options);
        times.setPromptText("00:00:00");
        sizeChosen.getItems().setAll("Small", "Medium", "Large");
        photoChosen.getItems().setAll("Hedgehog", "Horse", "Said");
    }

    @FXML
    protected String homeButtonAction() {
        return changeScene("homescreen");
    }

    @FXML
    protected String playButtonAction() {
        return changeScene("gamescreen");
    }

    @FXML
    protected String timeTrailButtonAction() {
        if (timeTrail.isSelected()) {
            timeTrailBox.getChildren().add(times);
            return "Activated the second player";
        } else {
            timeTrailBox.getChildren().remove(times);
            return "Deactivated the second player";
        }
    }

    @FXML
    protected String defaultPhotoButtonAction() {
        if (defaultPhoto.isSelected()) {
            loadPhoto.setSelected(false);
            photoChosen.setDisable(false);
            browsePhoto.setDisable(true);
            return "Deactivated the loadPhoto";
        } else {
            loadPhoto.setSelected(true);
            photoChosen.setDisable(true);
            browsePhoto.setDisable(false);
            return "Activated the loadPhoto";
        }
    }

    @FXML
    protected String loadPhotoButtonAction() {
        if (loadPhoto.isSelected()) {
            defaultPhoto.setSelected(false);
            photoChosen.setDisable(true);
            browsePhoto.setDisable(false);
            return "Deactivated the defaultPhoto";
        } else {
            System.out.println();
            defaultPhoto.setSelected(true);
            photoChosen.setDisable(false);
            browsePhoto.setDisable(true);
            return "Activated the defaultPhoto";
        }
    }

    @FXML
    protected String firstPlayerButtonAction() {
        if (secondPlayer.isSelected()) {
            secondPlayer.setSelected(false);
            secondPlayerName.setDisable(true);
            secondWASD.setDisable(true);
            secondKeys.setDisable(true);
            secondMouse.setDisable(true);
            return "Activated the second player";
        }
        return "Did nothing";
    }

    @FXML
    protected String secondPlayerButtonAction() {
        if (secondPlayer.isSelected()) {
            firstPlayer.setSelected(false);
            secondPlayerName.setDisable(false);
            secondWASD.setDisable(false);
            secondKeys.setDisable(false);
            secondMouse.setDisable(false);
            return "Activated the second player";
        } else {
            firstPlayer.setSelected(true);
            secondPlayerName.setDisable(true);
            secondWASD.setDisable(true);
            secondKeys.setDisable(true);
            secondMouse.setDisable(true);
            return "Deactivated the second player";
        }
    }

    @FXML
    protected String firstPlayerMode() {
        return "";
    }

    @FXML
    private String changeScene(String scene) {
        try {
            Parent homeScreenDiscard = FXMLLoader.load(getClass().getResource(scene + ".fxml"));
            Scene homeScreen = new Scene(homeScreenDiscard, 500, 600);
            Stage stageTheLabelBelongs = (Stage) title.getScene().getWindow();
            // Swap screen
            stageTheLabelBelongs.setScene(homeScreen);
            stageTheLabelBelongs.setTitle(scene.toUpperCase().charAt(0) + scene.toLowerCase().substring(1, scene.length() - 6) + " screen");
            return "Switched to " + scene;
        } catch (IOException IOE) {
            return "Whoops, couldn't load the new scene...";
        }
    }
}
